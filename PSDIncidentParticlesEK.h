#ifndef _PSDIncidentParticlesEK_PSDIncidentParticles_h_
#define _PSDIncidentParticlesEK_PSDIncidentParticles_h_

/**
 * \file
 * \author Evgeny Kashirin
 * \date 19 Mar 2021
 */

#include <fwk/VModule.h>
#include <boost/utility.hpp>

#include <map>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>

/*
 * Avoid using using namespace declarations in your headers,
 * doing so makes all symbols from each namespace visible
 * to the client which includes your header.
 */

namespace PSDIncidentParticlesEK {

  /**
  \class PSDIncidentParticles

  \brief Describe your module. In one sentence.

  Now here a longer description in doxygen. You may use HTML.


  \author Evgeny Kashirin
  \date 19 Mar 2021
  \version 
  \ingroup TODO: Put a group here.
  */

  class PSDIncidentParticles : 
      public boost::noncopyable,
      public fwk::VModule {
  public:
    PSDIncidentParticles() {}
    ~PSDIncidentParticles() {}
    fwk::VModule::EResultFlag Init();
    fwk::VModule::EResultFlag Process(evt::Event& event, const utl::AttributeMap& attr);
    fwk::VModule::EResultFlag Finish();
  private:
    struct QAStruct {
        TDirectory *output_dir{nullptr};
        TH2* h2_y_pt{nullptr};
        TH2* h2_phi_y{nullptr};
        TH2* h2_phi_pt{nullptr};
        TH2* h2_xy_psd{nullptr};

        void Merge(const QAStruct& other) {
            if (h2_y_pt && other.h2_y_pt) {
                h2_y_pt->Add(other.h2_y_pt);
            }
            if (h2_phi_y && other.h2_phi_y) {
                h2_phi_y->Add(other.h2_phi_y);
            }
            if (h2_phi_pt && other.h2_phi_pt) {
                h2_phi_pt->Add(other.h2_phi_pt);
            }
            if (h2_xy_psd && other.h2_xy_psd) {
                h2_xy_psd->Add(other.h2_xy_psd);
            }
        }

        void Write() {
            output_dir->cd();
            h2_y_pt->Write();
            h2_phi_y->Write();
            h2_phi_pt->Write();
            h2_xy_psd->Write();
        }
    };
    using PidModuleIndex = std::pair<int, int>;


    TFile *fQAFile{nullptr};
    std::map<PidModuleIndex, QAStruct> fQAMap;

    const std::vector<int> fModulesPSD1{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    std::map<int, QAStruct> fQA_psd1;
    const std::vector<int> fModulesPSD2{17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28};
    std::map<int, QAStruct> fQA_psd2;
    const std::vector<int> fModulesPSD3{29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44};
    std::map<int, QAStruct> fQA_psd3;

    // Declare down here your data following this conventions:
    // fFieldName  : members.
    // fgFieldName : static data members.

    // Declare down here your private functions like this:
    //
    // /// Brief description.
    // /*! Detailed description that can span several 
    //     lines.
    // */
    // type foo();

    // BEGIN EXAMPLE CODE (delete for your own module)
    double fVelocity;

    /// increases velocity
    void IncreaseSpeedByFactor(const double factor);
    // END EXAMPLE CODE (delete for your own module)

    // This goes at the end.
    REGISTER_MODULE("PSDIncidentParticlesEK", PSDIncidentParticles, "$Id$");
  };
}

#endif // _PSDIncidentParticlesEK_PSDIncidentParticles_h_

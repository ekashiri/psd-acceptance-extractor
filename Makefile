#
# General Makefile for a Shine module
#
#
# If the environment variable SHINEOFFLINEROOT is not set
# AND the executable 'shine-offline-config' is not in your PATH
# the definition of the following variable is required
#
# SHINEOFFLINEROOT = /usr/local/shine/OfflineInstall
#
# Replace the wildcard expression with .cc file list if you do
# not want to compile all .cc files in this directory
#
USER_SRCS = $(wildcard *.cc)
#
# All .xml.in files will be transformed into .xml with correct
# config and db locations
#
USER_XMLS = $(patsubst %.xml.in,%.xml,$(wildcard *.xml.in))
#
# Give your executable a name
#
EXE = userShineOffline
#
#############################################################

## You should not need to change anything below this line ###

# Authors: T. Paul, S. Argiro, L. Nellen, D. Veberic
# $Id$
# Send bug reports to http://www.shine.unam.mx/bugzilla/

.PHONY: all depend clean

ifdef SHINEOFFLINEROOT
  SHINEOFFLINECONFIG = $(SHINEOFFLINEROOT)/bin/shine-offline-config
else
  SHINEOFFLINECONFIG = shine-offline-config
endif

OBJS = $(USER_SRCS:.cc=.o)

CPPFLAGS    = $(shell $(SHINEOFFLINECONFIG) --cppflags)
CXXFLAGS    = $(shell $(SHINEOFFLINECONFIG) --cxxflags)
LDFLAGS     = $(shell $(SHINEOFFLINECONFIG) --ldflags)
MAIN        = $(shell $(SHINEOFFLINECONFIG) --main)
CONFIGFILES = $(shell $(SHINEOFFLINECONFIG) --config)
DBFILES     = $(shell $(SHINEOFFLINECONFIG) --db-path)

all: $(EXE) $(USER_XMLS)

$(EXE): $(OBJS)
	$(CXX) -o $@ $^ $(MAIN) $(CXXFLAGS) $(LDFLAGS)

%: %.in
	@echo -n "Generating $@ file..."
	@sed -e 's!@CONFIGDIR@!$(CONFIGFILES)!g;s!@SHINEDBDIR@!$(DBFILES)!g' $< >$@
	@echo "done"

#############################################################
# gcc can generate the dependency list

depend: Make-depend

Make-depend: $(USER_SRCS)
	$(CPP) $(CPPFLAGS) -MM $^ > $@

clean:
	- rm -f $(OBJS) $(EXE) $(USER_XMLS) Make-depend core run run_gdb

#############################################################
# 'make run' will run the thing

run: $(EXE) $(USER_XMLS)
	./$(EXE) -b bootstrap.xml && touch $@

#############################################################
# the lines below are for running with debugger 'make run_gdb'

.INTERMEDIATE: gdb.cmdl

# batch mode gdb needs a file with commands
gdb.cmdl:
	echo "r -b bootstrap.xml" > $@

run_gdb: gdb.cmdl $(EXE) $(USER_XMLS)
	gdb -batch -x $< ./$(EXE) && touch $@

-include Make-depend

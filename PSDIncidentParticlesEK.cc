#include "PSDIncidentParticlesEK.h"

#include <vector>

#include <TLorentzVector.h>

#include <fwk/CentralConfig.h>
#include <utl/Branch.h>
#include <utl/ErrorLogger.h>
#include <utl/ShineUnits.h>
#include <utl/PhysicalConst.h>
#include <utl/MagneticFieldTracker.h>
#include <evt/Event.h>
#include <evt/SimEvent.h>
#include <evt/RecEvent.h>

#include <det/Detector.h>
#include <det/PSD.h>


#include <sstream>

using namespace fwk;
using namespace utl;
using namespace std;
using namespace evt;

namespace PSDIncidentParticlesEK {

    // Define the methods preferibly in the same order as listed in the header file.

    VModule::EResultFlag 
        PSDIncidentParticles::Init()
        {
            CentralConfig& cc = CentralConfig::GetInstance();

            Branch topBranch = cc.GetTopBranch("PSDIncidentParticlesEK");
            InitVerbosity(topBranch);

            auto cwd = gDirectory;
            fQAFile = TFile::Open("qa.root", "recreate");
            /* recover gDirectory */

            auto init_qa_struct = [] (QAStruct &qa) {
                qa.output_dir->cd();
                qa.h2_y_pt = new TH2D("h2_y_pt", ";#it{y}_{CM};p_{T} (GeV/#it{c})",
                        400, -4., 4.,
                        200,  0., 2.);
                qa.h2_phi_y = new TH2D("h2_phi_y", ";#phi (rad);#it{y}_{CM}",
                        400, -4., 4.,
                        400, -4., 4.);
                qa.h2_phi_pt = new TH2D("h2_phi_pt", ";#phi (rad);p_{T} (GeV/#it{c}",
                        400, -4., 4.,
                        200,  0., 2.);
                qa.h2_xy_psd = new TH2D("h2_xy_psd", ";x (cm); y (cm)",
                        400, -100., 100.,
                        400, -100., 100.);
                return qa;
            };

            for (int pid : {2212, 211, -211}) {
                for (int im = 1; im < 45; ++im) {
                    auto index = std::make_pair(pid, im);
                    QAStruct qa;
                    qa.output_dir = fQAFile->mkdir(Form("pid_%d_%d", pid, im));
                    init_qa_struct(qa);
                    fQAMap.emplace(index, std::move(qa));
                } // modules

                QAStruct qa_psd1;
                qa_psd1.output_dir = fQAFile->mkdir(Form("pid_%d_psd1", pid));
                init_qa_struct(qa_psd1);
                fQA_psd1.emplace(pid, qa_psd1);
                
                QAStruct qa_psd2;
                qa_psd2.output_dir = fQAFile->mkdir(Form("pid_%d_psd2", pid));
                init_qa_struct(qa_psd2);
                fQA_psd2.emplace(pid, qa_psd2);

                QAStruct qa_psd3;
                qa_psd3.output_dir = fQAFile->mkdir(Form("pid_%d_psd3", pid));
                init_qa_struct(qa_psd3);
                fQA_psd3.emplace(pid, qa_psd3);
            } // pid

            gDirectory = cwd;
            return eSuccess;
        }

    VModule::EResultFlag
        PSDIncidentParticles::Process(evt::Event& event, const AttributeMap& /*attr*/)
        {

            std::set<Index<evt::sim::VertexTrack>> vtx_track_ind_set;

            const auto &sim_event = event.GetSimEvent();

            /* calclulate midrapidity */
            const auto &beam = sim_event.GetBeam();
            double midrapidity = [&beam] () {
                auto bm = beam.GetMomentum();
                TLorentzVector lv;
                lv.SetPxPyPzE(
                        bm.GetX() / GeV,
                        bm.GetY() / GeV,
                        bm.GetZ() / GeV,
                        TMath::Sqrt(utl::kProtonMass*kProtonMass + bm.GetMag2()) / GeV);
                return lv.Rapidity() / 2;
            }();

            std::cout << "Midrapidity = " << midrapidity << std::endl;

            /* PSD */
            const auto & det_psd = det::Detector::GetInstance().GetPSD();
            /* position of PSD face plane */
            const auto psd_Z = det_psd.GetPSDPositionZ () - det_psd.GetPSDLength()/2;
            const auto psd_X = det_psd.GetPSDPositionX ();
            const auto psd_Y = det_psd.GetPSDPositionY ();
            const auto psd_W = det_psd.GetPSDWidth();
            const auto psd_H = det_psd.GetPSDHeight();
            std::cout << "PSD: " << psd_X << " " << psd_Y << " " << psd_Z << std::endl;

            /* Magnetic field & tracker */
            const auto& magnetic_field = det::Detector::GetInstance().GetMagneticField();
            const auto& tracker = det::Detector::GetInstance().GetMagneticFieldTracker();


            auto sim_vtx_tracks_it = sim_event.Begin<evt::sim::VertexTrack>();
            const auto sim_vtx_tracks_end = sim_event.End<evt::sim::VertexTrack>();

            auto n_generated_vtx = 0;
            auto n_particles_of_interest = 0;
            auto n_fitted_to_psd_z = 0;
            auto n_acc_psd = 0;
            for (; sim_vtx_tracks_it != sim_vtx_tracks_end; ++sim_vtx_tracks_it) {
                if (sim_vtx_tracks_it->GetType() != evt::sim::VertexTrackConst::EType::eGeneratorFinal) {
                    continue;
                }

                if (!sim_vtx_tracks_it->HasStartVertex())
                    continue;

                n_generated_vtx++;

                auto particle_id = sim_vtx_tracks_it->GetParticleId();
                if (! (particle_id == 2212 || particle_id == 211 || particle_id == -211) )
                    continue;

                n_particles_of_interest++;


                auto position_at_vtx = sim_event.Get(sim_vtx_tracks_it->GetStartVertexIndex()).GetPosition();

                utl::Point position_at_psd_z;
                utl::Vector momentum_at_psd_z;
                auto tracking_ok = tracker.TrackToZ(psd_Z, sim_vtx_tracks_it->GetCharge(), position_at_vtx, sim_vtx_tracks_it->GetMomentum(), position_at_psd_z, momentum_at_psd_z);

                if (!tracking_ok)
                    continue;

                n_fitted_to_psd_z++;

                /* check if position_at_z as in the acceptance of PSD */
                if (!(TMath::Abs(position_at_psd_z.GetX() - psd_X) < (psd_W + 20.*cm)/2 && 
                            TMath::Abs(position_at_psd_z.GetY() - psd_Y) < (psd_H + 20.*cm)/2))
                    continue;
                n_acc_psd++;

                bool matched_module = false;
                int matched_module_no = -1;
                /* zero is PSD itself */
                for (int i_module = 1; i_module <= det_psd.GetNModules(); ++i_module) {
                    auto mod_X = det_psd.GetPositionX(i_module, 0);
                    auto mod_Y = det_psd.GetPositionY(i_module, 0);
                    auto mod_W = det_psd.GetWidth(i_module, 0);
                    auto mod_H = det_psd.GetHeight(i_module, 0);

                    if (TMath::Abs(position_at_psd_z.GetX() - (psd_X + mod_X)) < mod_W &&
                            TMath::Abs(position_at_psd_z.GetY() - (psd_Y + mod_Y)) < mod_H) {
                        matched_module = true;
                        matched_module_no = i_module;
                        break;
                    }
                } // psd modules

                if (!matched_module)
                    continue;

                /// collecting track' kinematics

                auto mass = [&particle_id] () {
                    if (particle_id == 2212)
                        return utl::kProtonMass;
                    else if (particle_id == 211 || particle_id == -211)
                        return utl::kPiChargedMass;
                    else
                        assert(false);
                    return 0.0;
                }();

                auto track_momentum = [&mass, &sim_vtx_tracks_it] () {
                    auto &momentum = sim_vtx_tracks_it->GetMomentum();
                    TLorentzVector lv;
                    lv.SetPxPyPzE(
                            momentum.GetX() / GeV,
                            momentum.GetY() / GeV,
                            momentum.GetZ() / GeV,
                            TMath::Sqrt(momentum.GetMag2() + mass*mass) / GeV);
                    return lv;
                }();

                auto pt = track_momentum.Pt();
                auto phi = track_momentum.Phi();
                auto y_cm = track_momentum.Rapidity() - midrapidity;
                std::cout << "Pt = " << pt << " (GeV/c);\tphi = " << phi << " (rad);\ty_cm = " << y_cm << std::endl;
                /// fill qa
                auto qa_it = fQAMap.find(std::make_pair(particle_id, matched_module_no));
                if (qa_it != fQAMap.end()) {
                    auto &qa = qa_it->second;
                    qa.h2_y_pt->Fill(y_cm, pt);
                    qa.h2_phi_y->Fill(phi, y_cm);
                    qa.h2_phi_pt->Fill(phi, pt);
                    qa.h2_xy_psd->Fill(position_at_psd_z.GetX() / cm, position_at_psd_z.GetY() / cm);
                }

            } // sim_vtx_tracks

            std::cout << "N(Sim Vtx from Generator) = " << n_generated_vtx << std::endl;
            std::cout << "N(Protons and pions) = " << n_particles_of_interest << std::endl;
            std::cout << "N(fitted to PSD Z) = " << n_fitted_to_psd_z << std::endl;
            std::cout << "N(around PSD acceptance) = " << n_acc_psd << std::endl;

            return eSuccess;
        }

    VModule::EResultFlag
        PSDIncidentParticles::Finish() 
        {
            for (auto &qa_element : fQAMap) {
                auto pid = qa_element.first.first;
                auto module_no = qa_element.first.second;
                if (std::find(fModulesPSD1.begin(), fModulesPSD1.end(), module_no) != fModulesPSD1.end()) {
                    fQA_psd1[pid].Merge(qa_element.second);
                }   
                if (std::find(fModulesPSD2.begin(), fModulesPSD2.end(), module_no) != fModulesPSD2.end()) {
                    fQA_psd2[pid].Merge(qa_element.second);
                }   
                if (std::find(fModulesPSD3.begin(), fModulesPSD3.end(), module_no) != fModulesPSD3.end()) {
                    fQA_psd3[pid].Merge(qa_element.second);
                }   

                qa_element.second.Write();
            }

            for (auto &qa_element : fQA_psd1) {
                qa_element.second.Write();
            }
            for (auto &qa_element : fQA_psd2) {
                qa_element.second.Write();
            }
            for (auto &qa_element : fQA_psd3) {
                qa_element.second.Write();
            }



            fQAFile->Close();


            return eSuccess;
        }

    // BEGIN EXAMPLE CODE (delete for your own module)
    void
        PSDIncidentParticles::IncreaseSpeedByFactor(const double factor)
        {
            fVelocity *= factor;
        }
    // END EXAMPLE CODE (delete for your own module)

}
